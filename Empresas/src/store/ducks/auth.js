export const Types = {
    REQUEST: 'auth/REQUEST',
    SUCCESS: 'auth/SUCCESS',
    FAILURE: 'auth/FAILURE',
    LOGOUT: 'auth/LOGOUT',
    CHANGE_USER: 'auth/CHANGE_USER',
  };
  
  const initialState = {
    isLogged: false,
    requesting: false,
    accessToken: null,
    uid: null,
    client: null,
    errorMessage: [],
    successAuth: false,
  };
  
  export default function reducer(state = initialState, action) {
    switch (action.type) {
      case Types.REQUEST:
        return {...state, requesting: true};
      case Types.SUCCESS:
        console.log('DUCKS AUTH', action.payload);
        return {
          ...state,
          accessToken: action.payload.accessToken,
          uid: action.payload.uid,
          client: action.payload.client,
          isLogged: true,
          successAuth: true,
          requesting: false,
          error: false,
        };
      case Types.FAILURE:
        return {
          ...state,
          requesting: false,
          errorMessage: action.payload.errors,
        };
      case Types.LOGOUT:
        return {...state};
      default:
        return state;
    }
  }
  
  export function request(params) {
    return {
      type: Types.REQUEST,
      payload: params,
    };
  }
  
  export function success({accessToken, uid, client}) {
    return {
      type: Types.SUCCESS,
      payload: {accessToken, uid, client},
    };
  }
  
  export function failure(errors) {
    return {
      type: Types.FAILURE,
      payload: errors,
    };
  }
  
  export function logout() {
    return {
      type: Types.LOGOUT,
    };
  }
  
  