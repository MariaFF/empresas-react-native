import React from 'react';
import { ActivityIndicator } from 'react-native';
import  Icon  from 'react-native-vector-icons/Feather';

import { Container, Content, ButtonText } from './styles';

const Button = ({ text, submit, loading, icon, ...rest }) => {
  return (
    <Container onPress={() => submit()} {...rest}>
      {
        loading ?
          (
            <ActivityIndicator size="small" color="#fff" />
          ) :
          (
            <Content>
              {icon && (<Icon name={icon} size={22} color="#fff" style={{ marginRight: 8 }} />)}              
              <ButtonText>{text}</ButtonText>
            </Content>
          )
      }

    </Container>
  );
};

export default Button;
