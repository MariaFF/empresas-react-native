import React, {
  useRef,
  useImperativeHandle,
  useState,
  useEffect,
  forwardRef,
  useCallback,
} from 'react';
import { useField } from '@unform/core';

import { Container, TextInput, Icon, IconPass, MessageErrorForm } from './styles';

const Input = (
  { testID, name, icon, iconPass, handleShowPass, ...rest },
  ref,
) => {
  const inputElementRef = useRef(null);
  const { registerField, defaultValue = '', fieldName, error } = useField(name);
  const inputValueRef = useRef({ value: defaultValue });
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);
    setIsFilled(!!inputValueRef.current.value);
  }, []);

  const handleChangeText = useCallback((value) => {
    inputValueRef.current.value = value;
  }, []);

  // Customiza as propriedades do objeto que retorna para o comp pai quando usamos o Ref
  useImperativeHandle(ref, () => ({
    focus() {
      inputElementRef.current.focus();
    },
  }));

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputValueRef.current,
      path: 'value',
      setValue(ref, value) {
        inputValueRef.current.value = value;
        // mudar visual o texto que ta dentro do input
        inputElementRef.current.setNativeProps({ text: value });
      },
      clearValue() {
        inputValueRef.current.value = '';
        inputElementRef.current.clear();
      },
    });
  }, [fieldName, registerField]);

  return (
    <>
      {error && !isFilled && <MessageErrorForm>{error}</MessageErrorForm>}
      <Container isFocused={isFocused} isFilled={isFilled} isErrored={!!error}>
        <Icon name={icon} size={20} />
        <TextInput
          testID={testID}
          ref={inputElementRef}
          keyboardAppearance="dark"
          placeholderTextColor="#fff"
          defaultValue={defaultValue}
          onFocus={handleInputFocus}
          onBlur={handleInputBlur}
          onChangeText={(value) => {
            handleChangeText(value);
          }}
          {...rest}
        />
        {fieldName === 'senha' && (
          <IconPass onPress={() => handleShowPass()} name={iconPass} size={22} />
        )}
      </Container>
    </>
  );
};

export default forwardRef(Input);
