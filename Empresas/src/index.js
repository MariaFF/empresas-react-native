import 'react-native-gesture-handler';
import React from 'react';
import {
  StatusBar,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';

import Routes from './routes'
import store from './store'


const App = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle="light-content" backgroundColor="#007EF2" />
      <Provider store={store}>
        <Routes />
      </Provider>
    </NavigationContainer>
  );
};


export default App;
