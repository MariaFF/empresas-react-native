import React, {useCallback, useEffect, useMemo, useState} from 'react';
import { FlatList, View, Text, SafeAreaView, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Modal from 'react-native-modal'

import * as EnterpriseActions from '../../store/ducks/enterprise';

import EmptyList from '../../components/Enterprise/EmptyList'

import Icon from 'react-native-vector-icons/Feather'
import { 
  Container,
  Content,
  Header,
  Title,
  HeaderTitle,
  ListItem,
  EnterpriseInfo,
  EnterpriseName,
  EnterpriseLocation,
  Image,
  Chip,
  ChipText,
} from './styles';
import Button from '../../components/Button';
import Search from '../../components/Enterprise/Search';
import { useNavigation } from '@react-navigation/native';


const Enterprise = () => {
  const  uid = useSelector(state => state.auth.uid);
  const  client = useSelector(state => state.auth.client);
  const  accessToken = useSelector(state => state.auth.accessToken);
  const loading = useSelector(state => state.enterprise.requesting);
  const enterprises = useSelector(state => state.enterprise.enterprises);

  const [modalVisible, setModalVisible] = useState(false);
  const [isSearching, setIsSearching] = useState(false);
  const [resultSearch, setResultSearch] = useState();
  const [isEmptyResultSearch, setIsEmptyResultSearch] = useState(false)

  const navigation = useNavigation()
  const dispatch = useDispatch();


  useEffect(() => {
    if (accessToken) {
      async function fetchEnterprises() {
        try {
          dispatch(EnterpriseActions.request({uid, client, 'access-token': accessToken}))
        } catch (error) {
          console.log('Erro Horários', error);
          if (error.response && error.response.data) {
            console.log('Erro Horários', error.response.data);
          }
        }
      }
      fetchEnterprises();
    }
  }, [accessToken])

  const enterprisesFake =
     [
        {
            "id": 1,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "Fluoretiq Limited",
            "photo": "/uploads/enterprise/photo/1/240.jpeg",
            "description": "FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.  ",
            "city": "Bristol",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 3,
                "enterprise_type_name": "Health"
            }
        },
        {
            "id": 2,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "Little Bee Community",
            "photo": "/uploads/enterprise/photo/2/240.png",
            "description": "Service and Product Design, Responsible Business",
            "city": "London",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 12,
                "enterprise_type_name": "Service"
            }
        },
        {
            "id": 3,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "HSQ Productions",
            "photo": "/uploads/enterprise/photo/3/240.jpeg",
            "description": "Personality personalised 'Done in a Day' video production ",
            "city": "Manchester",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 12,
                "enterprise_type_name": "Service"
            }
        },
        {
            "id": 4,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "VendorLink",
            "photo": "/uploads/enterprise/photo/4/240.jpeg",
            "description": "We are reinventing the sales channel through our bespoke platform, myvendorlink, which aims to bring the best niche, new and emerging technology directly to IT resellers - and vice versa!",
            "city": "Alton",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 7,
                "enterprise_type_name": "IT"
            }
        },
        {
            "id": 5,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "TOAD.ai",
            "photo": "/uploads/enterprise/photo/5/240.jpeg",
            "description": "TOAD.ai is an online tool for businesses and ad agencies looking for a quick way to plan and buy outdoor advertising campaigns. Rather than contacting multiple media owners or going to slow and expensive media planners, TOAD generates proposals in seconds, saving time and money.\n\nAdvertising signage is a powerful medium that dominates our built environment yet it lacks a consolidated trading platform. By combining the UK’s advertising signage onto a single platform and layering it with audience data, behavioural insights and planning automation, TOAD.ai lets you book campaigns optimized to reach target audiences.\n\nTHINK BIGGER!\n",
            "city": "London",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 21,
                "enterprise_type_name": "Marketplace"
            }
        },
        {
            "id": 6,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "Veuno Ltd.",
            "photo": "/uploads/enterprise/photo/6/240.jpeg",
            "description": "Problem identified: People have difficulties managing money and coordinating across multiple accounts.\n\nSolution: Bring all accounts in one place for easier management using a goal-based approach and third party service recommendations for efficient spending, everything inside a mobile app.\n\nOur goal is to help people get more value from their money.",
            "city": "London",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 2,
                "enterprise_type_name": "Fintech"
            }
          },
          {
            "id": 7,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "Fluoretiq Limited",
            "photo": "/uploads/enterprise/photo/1/240.jpeg",
            "description": "FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.  ",
            "city": "Bristol",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 3,
                "enterprise_type_name": "Health"
            }
        },
        {
            "id": 8,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "Little Bee Community",
            "photo": "/uploads/enterprise/photo/2/240.png",
            "description": "Service and Product Design, Responsible Business",
            "city": "London",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 12,
                "enterprise_type_name": "Service"
            }
        },
        {
            "id": 9,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "HSQ Productions",
            "photo": "/uploads/enterprise/photo/3/240.jpeg",
            "description": "Personality personalised 'Done in a Day' video production ",
            "city": "Manchester",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 12,
                "enterprise_type_name": "Service"
            }
        },
        {
            "id": 10,
            "email_enterprise": null,
            "facebook": null,
            "twitter": null,
            "linkedin": null,
            "phone": null,
            "own_enterprise": false,
            "enterprise_name": "VendorLink",
            "photo": "/uploads/enterprise/photo/4/240.jpeg",
            "description": "We are reinventing the sales channel through our bespoke platform, myvendorlink, which aims to bring the best niche, new and emerging technology directly to IT resellers - and vice versa!",
            "city": "Alton",
            "country": "UK",
            "value": 0,
            "share_price": 5000.0,
            "enterprise_type": {
                "id": 7,
                "enterprise_type_name": "IT"
            }
        },
    ]

    const renderImage = (enterprise) => {
      if (enterprise.photo) {
        return (
          <Image source={{ uri: `http://empresas.ioasys.com.br${enterprise.photo}` }} />
        )
      }
      return (
        <Text>{enterprise.name[0]}</Text>
      )
    }

    const renderListItem = useCallback(({item}) => {
      return (
        <ListItem onPress={() => {
          console.log('Item', item)
          navigation.navigate('Details', {item})
        }}>
          <Chip>
            <ChipText>{item.enterprise_type.enterprise_type_name}</ChipText>
          </Chip>
          {renderImage(item)}
          <EnterpriseInfo>
            <EnterpriseName>{item.enterprise_name}</EnterpriseName>
            <EnterpriseLocation>{`${item.city} - ${item.country}`}</EnterpriseLocation>
          </EnterpriseInfo>
        </ListItem>
      )
    }, [])


  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Header>
          {resultSearch && (<Icon name="arrow-left" size={24} onPress={() => setResultSearch(null)} />)}
          <HeaderTitle>
            <Title>Empresas</Title>
          </HeaderTitle>
        </Header>
        {loading ? 
          (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator color="#007ef2" size="large" />
          </View>
          ) :
          (
            <>
              {!isSearching && (
                <Button
                style={{ height: 50 }}
                text={'Pesquisar empresas'}
                icon={"search"}
                submit={() => setModalVisible(true)}
              />
              )}
              
              <FlatList 
                data={resultSearch ? resultSearch : enterprisesFake}
                keyExtractor={(item) => item.id.toString()}
                renderItem={renderListItem}
              />
            </>
          )
        }
      </Container>
        <Modal
        animationOut="fadeOutDown"
        backdropColor="#16335f"
        backdropOpacity={0.8}
        isVisible={modalVisible}
        style={{ flex: 1 }}
        onBackButtonPress={() => setModalVisible(false)}
        onBackdropPress={() => setModalVisible(false)}>
          <Search setResultSearch={setResultSearch} setIsEmptyResultSearch={setIsEmptyResultSearch} setModalVisible={setModalVisible} />
        </Modal>
    </SafeAreaView>
  );
}

export default Enterprise;