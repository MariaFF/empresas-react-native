import styled from 'styled-components/native';

export const Container = styled.View`
  
`;

export const Header = styled.View`
  flex-direction: row;
  align-items: center;
  height: 50px;
  padding: 0 8px;
`;

export const Overlay = styled.View`
  flex: 1;
  width: 100%;
  height: 300px;
  position: absolute;
  opacity: 0.4;
  background: #16335f;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`;