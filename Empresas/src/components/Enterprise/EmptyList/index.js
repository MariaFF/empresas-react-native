import React from 'react';

import {
  Container,
  TextInfo,
} from './styles';

import Button from '../../../components/Button/index';
import {useNavigation} from '@react-navigation/native';

const EmptyList = () => {
  const navigation = useNavigation();

  return (
    <Container >
			<TextInfo>Nenhuma empresa cadastrada</TextInfo>
			<Button
				style={{height: 50, width: '90%'}}
				text={'Cadastrar nova empresa'}
				// submit={showFormCardModal}
			/>
      
    </Container>
  );
};

export default EmptyList;
