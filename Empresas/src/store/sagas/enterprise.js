import {takeLatest, call, put} from 'redux-saga/effects';
import {Types} from '../ducks/enterprise';
import * as EnterpriseActions from '../ducks/enterprise';
import api from '../../services/api'

const fetchEnterpriseRequest = async (params) => {
  console.log('EMPRESA FETCHHH PARAMS', params);
  return await api.get('/enterprises', {params});
};

function* fetchEnterprises({payload}) {
  try {
    const response = yield call(fetchEnterpriseRequest, payload);
    console.log('EMPRESA DEU CERTO');
    yield put(EnterpriseActions.success(response.data.enterprises));
  } catch (error) {
    console.log('EMPRESA ERR', error);
    if (error.response && error.response.data) {
        console.log('EMPRESA ERR if', error.response.data);
      yield put(EnterpriseActions.failure(error.response.data));
    }
  }
}

export default function* () {
  yield takeLatest(Types.REQUEST, fetchEnterprises);
}
