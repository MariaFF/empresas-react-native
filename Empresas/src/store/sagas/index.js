import { all } from 'redux-saga/effects';
import auth from './auth';
import enterprise from './enterprise'

export default function* rootSaga() {
  yield all([auth(), enterprise()]);
}
