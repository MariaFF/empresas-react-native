import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  background: #007ef2;
  height: 45px;
  width: 100%;
  border-radius: 3px;
  align-items: center;
  justify-content: center;
  margin-top: 8px;
`;

export const Content = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: center
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 16px;
`;
