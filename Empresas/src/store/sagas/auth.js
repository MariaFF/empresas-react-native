import {takeLatest, call, put} from 'redux-saga/effects';
import {Types} from '../ducks/auth';
import * as AuthActions from '../ducks/auth';
import api from '../../services/api'

const authenticateRequest = async (params) => {
  return await api.post('/users/auth/sign_in', params);
};

function* authenticate({payload}) {
  try {
    const response = yield call(authenticateRequest, payload);
    const {'access-token': accessToken, uid, client } = response.headers
    console.log('LOGIN DEU CERTO', response.headers);
    api.defaults.headers = { uid, client, 'access-token': accessToken };
    yield put(AuthActions.success({ accessToken, uid, client }));
  } catch (error) {
    console.log('AUTH ERR', error);
    if (error.response && error.response.data) {
        console.log('AUTH ERR', error.response.data);
      yield put(AuthActions.failure(error.response.data));
    }
  }
}

export default function* () {
  yield takeLatest(Types.REQUEST, authenticate);
}
