export const Types = {
    REQUEST: 'enterprise/REQUEST',
    SUCCESS: 'enterprise/SUCCESS',
    FAILURE: 'enterprise/FAILURE',
  };
  
  const initialState = {
    requesting: false,
    enterprises: [],
    errorMessage: [],
    success: false,
  };
  
  export default function reducer(state = initialState, action) {
    switch (action.type) {
      case Types.REQUEST:
        return {...state, requesting: true};
      case Types.SUCCESS:
        // console.log('DUCK', action.payload);
        return {
          ...state,
          enterprises: action.payload,
          success: true,
          requesting: false
        };
      case Types.FAILURE:
        return {
          ...state,
          requesting: false,
          errorMessage: action.payload.errors,
        };
      default:
        return state;
    }
  }
  
  export function request(params) {
    return {
      type: Types.REQUEST,
      payload: params,
    };
  }
  
  export function success(enterprises) {
    return {
      type: Types.SUCCESS,
      payload: enterprises,
    };
  }
  
  export function failure(errors) {
    return {
      type: Types.FAILURE,
      payload: errors,
    };
  }
  
  