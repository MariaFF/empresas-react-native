import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignIn from './pages/SignIn';
import Enterprise from './pages/Enterprise'
import Details from './pages/Details';

const AppStack = createStackNavigator();

const Routes = () => {
  return (
    <AppStack.Navigator
      screenOptions={{
        headerShown: false,
        cardStyle: { backgroundColor: '#FFF', flex: 1 },
      }}>
      <AppStack.Screen name="SignIn" component={SignIn} />
      <AppStack.Screen name="Enterprise" component={Enterprise} />
      <AppStack.Screen name="Details" component={Details} />
      </AppStack.Navigator>
  );
};

export default Routes;