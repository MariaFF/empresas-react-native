function getValidationErrors(err) {
    const validationErrors = {};
  
    err.inner.forEach((erro) => {
      validationErrors[erro.path] = erro.message;
    });
    return validationErrors;
  }
  
  export default getValidationErrors;
  