![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações do projeto Empresas.

---

### Dependencias ###

* Navegação
	* "@react-native-community/masked-view": "^0.1.10"
	* "@react-navigation/native": "^5.9.2"
	* "@react-navigation/stack": "^5.14.2"
	* "react-native-reanimated": "^1.13.2"
	* "react-native-gesture-handler": "^1.10.1"
* Api Rest
	* Axios
* Estilização e validação
	* styled-components
	* yup - validação de campo
	* "@unform/core": "^1.9.8" - manipulação de formulários
    * "@unform/mobile": "^1.9.8"
	* "react-native-vector-icons": "^8.0.0",
* Gerenciamento de estado da aplicação
	* react-redux
	* redux
	* redux-saga


### Funcionalidades
* Login

* Endpoints disponíveis:
	* Listagem de Empresas: `/enterprises` - a listagem não está usando os dados retornados da api, foram mocados
	* Detalhamento de Empresas: `/enterprises/{id}` - ok
	* Filtro de Empresas por nome e tipo: `/enterprises?enterprise_types={type}&name={name}` - ok

### Executar o projeto
* cd Empresas
* yarn - instalar as dependencias
* cd ios
* pod install
* cd ..
* yarn ios
