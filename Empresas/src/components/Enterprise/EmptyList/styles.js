import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const TextInfo = styled.Text`
  color: #16335f;
  font-size: 16px;
  text-align: center;
  margin-top: 16px;
`;

