import styled, { css } from 'styled-components/native';
import FeatherIcon from 'react-native-vector-icons/Feather';

export const Container = styled.View`
  background: #305185;
  flex-direction: row;
  height: 50px;
  width: 100%;
  border-radius: 4px;
  margin-top: 8px;
  margin-bottom: 8px;
  align-items: center;
  padding-left: 8px;

  ${(props) =>
    props.isErrored && !props.isFilled &&
    css`
      border-width: 1px;
      border-color: #c53030;
    `}

`;

export const TextInput = styled.TextInput`
  font-size: 16px;
  color: #fff;
  flex: 1;
`;

export const Icon = styled(FeatherIcon)`
  margin: 0 8px;
  color: #fff;
`;

export const IconPass = styled(FeatherIcon)`
  margin: 0 8px;
  color: #fff;
`;

export const MessageErrorForm = styled.Text`
  color: #e53e3e;
  font-size: 14px;
  align-self: flex-end;
`;
