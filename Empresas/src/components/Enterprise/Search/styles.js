import styled from 'styled-components/native';

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
`;

export const ModalContainer = styled.View`
  background: #fff;
  width: 80%;
  border-radius: 4px;
  padding: 16px;
`;

export const Title = styled.Text`
  color: #000;
  font-size: 18px;
  align-self: center;
`;

export const Label = styled.Text`
  color: #000;
  font-size: 14px;
  margin-top: 8px;
`;

export const PickerContainer = styled.View`
  height: 50px;
  width: 100%;
  background: #edf2f7;
  border-radius: 4px;
  margin: 4px 0;
  padding-left: 8px;
  justify-content: center;
`;
