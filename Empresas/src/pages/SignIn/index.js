import React, { useRef, useState, useCallback, useEffect } from 'react';
import { SafeAreaView, ScrollView, Text, Alert, Image } from 'react-native';
import { Form } from '@unform/mobile';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import * as Yup from 'yup';

import * as AuthActions from '../../store/ducks/auth';

import getValidationErrors from '../../utils/getValidationErrors'
import Input from '../../components/Input/index'
import Button from '../../components/Button/index'
import { Container } from './styles';

const SignIn = () => {
	const formRef = useRef(null);
  const emailInputRef = useRef(null);
  const senhaInputRef = useRef(null);
	const [showPass, setShowPass] = useState(false);
	const [loading, setLoading] = useState(false)

	const successAuth = useSelector((state) => state.auth.successAuth);
  const errorMessage = useSelector((state) => state.auth.errorMessage);
	const navigation = useNavigation()
	const dispatch = useDispatch()

	useEffect(() => {
    if (successAuth) {
      navigation.navigate('Enterprise');
    }
    if (errorMessage.length > 0) {
      return Alert.alert(
        'Login',
        errorMessage[0],
        [{ text: 'OK', onPress: () => 'OK Pressed' }],
        { cancelable: true },
      );
    }
  }, [successAuth, errorMessage]);

	const handleShowPass = useCallback(() => {
    if (showPass) {
      setShowPass(false);
    } else {
      setShowPass(true);
    }
  }, [showPass]);

	const handleSignIn = async(data) => {
		try {
			const schema = Yup.object().shape({
        email: Yup.string()
          .required('Email obrigatório')
          .email(),
        password: Yup.string()
          .required('Informe sua senha'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });
			dispatch(AuthActions.request({...data}))
		} catch (error) {
			console.log('error', error);
		 if (error instanceof Yup.ValidationError) {
          const errors = getValidationErrors(error);
          formRef.current.setErrors(errors);
			}
			return Alert.alert(
        'Login falhou',
        error,
        [{ text: 'OK', onPress: () => 'OK Pressed' }],
        { cancelable: true },
      );
		}
	}

  return (
		<SafeAreaView style={{ flex: 1 }}>
			<ScrollView style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
				<Container>
					<Image style={{ alignSelf: 'center', marginVertical: 16 }} source={require('../../assets/logo_ioasys.png')} />
					<Form ref={formRef} style={{ flex: 1 }} onSubmit={handleSignIn}>
					<Input
						name="email"
						icon="mail"
						autoFocus={true}
						ref={emailInputRef}
						placeholder="Email"
						autoCorrect={false}
						autoCapitalize="none"
						keyboardType="email-address"
						returnKeyType="next"
						onSubmitEditing={() => {
							senhaInputRef.current.focus();
						}}
					/>

					<Input
						name="password"
						icon="lock"
						ref={senhaInputRef}
						placeholder="Senha"
						autoCorrect={false}
						secureTextEntry={!showPass}
						returnKeyType="send"
						iconPass={showPass ? 'eye-off' : 'eye'}
						handleShowPass={handleShowPass}
						onSubmitEditing={() => {
							formRef.current.submitForm();
						}}
					/>

					<Text
						onPress={() => {}}
						style={{
							color: '#000',
							marginTop: 4,
						}}>
						Esqueceu sua senha ?
					</Text>

					<Button
              style={{ height: 50 }}
              disabled={loading}
              loading={loading}
              text={'Entrar'}
              submit={() => {formRef.current.submitForm()}}
            />
					</Form>
				</Container>
			</ScrollView>
		</SafeAreaView>
  );
}

export default SignIn;