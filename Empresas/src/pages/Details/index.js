import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { View, ImageBackground, Text, ScrollView, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/Feather'

import api from '../../services/api';

import { Container, Header, Overlay } from './styles';

const Details = ({route}) => {
  const {item} = route.params;
  const navigation = useNavigation();
  const [enterprise, setEnterprise] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getEnterprise() {
      if (item) {
        try {
          const {data} = await api.get(`/enterprises/${item.id}`)
          if (data) {
            setEnterprise(data.enterprise);
            setLoading(false);
          }
        } catch (error) {
          console.log('Erro', error);
          if (error.response && error.response.data) {
            setLoading(false)
            console.log('Erro', error.response.data);
          }
        }
      }
    }
    getEnterprise()
  }, [item])

    return (
      loading ? 
      (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <ActivityIndicator size="large" color="#16335f" />
        </View>
      ) :
      (
        <>
          <ImageBackground
            imageStyle={{ borderBottomLeftRadius: 26, borderBottomRightRadius: 26 }}
            style={{ height: 300, position: 'relative' }}
            source={{ uri:  `http://empresas.ioasys.com.br${enterprise.photo}` }}
          >

           <Overlay>
              <Header style={Platform.OS === 'ios' && { marginTop: 20 }}>
                <Icon name="arrow-left" size={24} color="#fff" onPress={() => navigation.goBack()} />
              </Header>
            </Overlay>
          </ImageBackground>
          <View style={{
            flex: 1,
            paddingHorizontal: 16,
            paddingVertical: 8
          }}
          >
            <View style={{ flexDirection: 'row', backgroundColor: '#fff', }}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: '#16335f', fontSize: 18 }}>{enterprise.enterprise_name}</Text>
                <Text style={{ color: '#415d87', fontSize: 14 }}>{enterprise.enterprise_type.enterprise_type_name}</Text>
              </View>
            </View>
            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center' }}>
              <Text>
                {enterprise.description}
              </Text>
            </ScrollView>
          </View>
        </>
      )
      );
}

export default Details;