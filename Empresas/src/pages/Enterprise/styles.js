import { Pressable } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  padding: 16px;
`;

export const Header = styled.View`
  height: 50px;
  flex-direction: row;
  align-items: center;
`;

export const HeaderTitle = styled.View`
  flex: 1;
  margin-right: 16px;
  align-items: center;
`;

export const Title = styled.Text`

`;

export const ListItem = styled.TouchableOpacity`
  flex: 1;
  background: #f5f5f5;
  flex-direction: row;
  margin-top: 24px;
  padding: 16px;
  border-radius: 4px;
  align-items: center;
`;

export const Image = styled.Image`
  height: 60px;
  width: 60px;
  border-radius: 30px;
`;

export const EnterpriseInfo = styled.View`
  justify-content: space-between;
  height: 50px;
  margin-left: 8px;
`;

export const EnterpriseName = styled.Text`
  font-size: 16px;

`;

export const EnterpriseLocation = styled.Text`

`;

export const Chip = styled.View`
  position: absolute;
  /* bottom: 80px; */
  top: -10px;
  right: 10px;
  bottom: 80px;
  border-radius: 4px;
  padding: 2px 16px;
  align-items: center;
  background: #007ef2;
  width: 120px;
`;

export const ChipText = styled.Text`
  color: #fff;
  font-size: 12px;
`;

