import React, {useMemo, useState} from 'react';
import { View, Text } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { useSelector } from 'react-redux';
import Button from '../../Button';

import api from '../../../services/api'

import { Container, ModalContainer, Title, PickerContainer, Label } from './styles';

const Search = (props) => {
  const {setResultSearch, setModalVisible, setIsEmptyResultSearch} = props;
  const [idType, setIdType] = useState();
  const enterprises = useSelector(state => state.enterprise.enterprises);

  const [type, setType] = useState()
  const [name, setName] = useState()

 const types = useMemo(() => {
  if (enterprises.length === 0) return;

  let reduced = [];
      enterprises.forEach((enterprise) => {
        const {enterprise_type_name, id} = enterprise.enterprise_type
        var duplicated = reduced.findIndex(redItem => {
          return id == redItem.id;
        }) > -1;

        if (!duplicated) {
          reduced.push({
            name: enterprise.enterprise_name,
            id,
            type: enterprise_type_name,
            label: enterprise_type_name,
            value: id,
          });
        }
      });
      return reduced;
 }, [enterprises]);

  const names = useMemo(() => {
    if (!idType) return [{value: 'Selecione o nome', label: ''}];

    return types.filter((item) => item.id === idType ).map(item => ({...item, value: item.name, label: item.name}))
  }, [idType, types])

  const handleSearch = async() => {
    try {
      const params = {enterprise_types: type, name}
      const response = await api.get('/enterprises', {params})  
      if (response && response.data.enterprises.length === 1) {
        setResultSearch(response.data.enterprises);
        setIsEmptyResultSearch(false)
      } else {
        setIsEmptyResultSearch(true)
      }
      setModalVisible(false);
    } catch (error) {
      console.log('ERROR', error);
      if (error.response && error.response.data) {
        console.log('ERROR', error.response.data);
      }
    }
    
  }

  return (
    <Container>
      <ModalContainer>
        <Title>Buscar empresa</Title>
        <Label>Tipo empresa</Label>
        <PickerContainer>
          <RNPickerSelect
            onValueChange={(value) => {
              setIdType(value)
              setType(value)
            }}
            items={types}
            useNativeAndroidPickerStyle={false}
          />
        </PickerContainer>

        <Label>Nome empresa</Label>
        <PickerContainer>
          <RNPickerSelect
            onValueChange={(value) => setName(value)}
            items={names}
            useNativeAndroidPickerStyle={false}
          />
        </PickerContainer>

        <Button
          style={{ height: 50 }}
          icon={"search"}
          text={'Pesquisa'}
          submit={handleSearch}
        />
      </ModalContainer>
    </Container>
  );
}

export default Search;