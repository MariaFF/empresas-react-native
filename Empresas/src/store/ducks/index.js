import { combineReducers } from 'redux';
import auth from './auth';
import enterprise from './enterprise'

const reducers = combineReducers({
  auth, enterprise,
});

export default reducers;
